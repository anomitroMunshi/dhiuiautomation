/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */


const faker = require("faker");

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on("task", {
    freshUser() {
        user = {
            username: faker.name.firstName(),
            email: faker.internet.email(),
            accountName: faker.company.companyName(),
            landingpageurl: faker.internet.url(),
            username_obs: faker.name.lastName(),
            email_obs: faker.internet.email()+"obs",
            StoryName: faker.random.word()+faker.finance.creditCardCVV(),
            StoryLocalname: faker.vehicle.model(),
            TagName: faker.lorem.word()+"Tag",
            CampaignName: "Campaign " +faker.random.alphaNumeric()+faker.finance.creditCardCVV(),
            RuleName: "Rule"+faker.random.word()
        };
        return user;
    },
});
}
