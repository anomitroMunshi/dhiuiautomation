class storyAds{

    getPageTitle(){
        return cy.get('h4')
    }

    getCreateStoryAdsBtn(){
        return cy.get('#create-story-ads')
    }

    getFirstStory(){
        return cy.get('#paper-0')
    }

    getSelectedicon(){
        return cy.get('.overlay > .las')
    }

    getApplyBtnWhileCreatingStory(){
        return cy.get('.story-selection__nav > .button')
    }

    getFrameSelectionList(){
        return cy.get('.frame-selection__list')
    }

    getStoryAdFrameApply(){
        return cy.get('#story-ad-frame-apply')
    }


    selectThirdLogo(){
        return cy.get('#brand-logo-2 > img')
    }

    getinputBoxForStoryAd(){
        return cy.get('#input-text-0')
    }

    getSubtitileinputBoxforStoryAd(){
        return cy.get('#input-text-1')
    }

    getAddTextlabel(){
        return cy.get('#add-text')
    }

    getStoryAdThemesList(){
        return cy.get('#story-ad-themes')
    }

    get300x600Btn(){
        return cy.get('#size-300x600')
    }

    get300x250Btn(){
        return cy.get('#size-300x250')
    }

    getEditStoryAdNamelabelBtn(){
        return cy.get('.summary__text > div > .las')
    }

    getInputboxStoryAdName(){
        return cy.get('#edit-name-input')
    }

    saveStoryAdname(){
        return cy.get('#edit-ad-name')
    }

    saveAds(){
        return cy.get('#save-ads')
    }

    getFontContainer(){
        return cy.get('#select2-fonts-container')
    }
    getFontInputBox(){
        return cy.get('.select2-search__field')
    }

}

export default storyAds;