class createStory{

    getStoryPageTitle(){
        return cy.get('.story-header>div>h4')
    }

    getCreateStoryBtn(){
        return cy.get('#create-story')
    }



    getStoryDisplayNamebox(){
        return cy.get('.sidebar > .body > :nth-child(1) > .form-fieldset > :nth-child(1) > :nth-child(1) > #story-display-name')
    }

    getLocalName(){
        return cy.get('.sidebar > .body > :nth-child(1) > .form-fieldset > :nth-child(1) > :nth-child(2) > #local-name')
    }

    getCreatebtn(){
        return cy.get('.sidebar > .body > :nth-child(1) > .form-fieldset > .mg-t-10 > .row > .col-sm-4 > #create-story-btn')
    }

    getStoryList(){
        return cy.get('.story-item>div')
    }

    getFirstStorynameFromList(){
        return cy.get('.story-item>div>div:nth-child(1)>.story-listing-cont-footer>p')
    }

    getFirstStoryMenu(){
        return cy.get('.story-item>div>div:nth-child(1)>.story-listing-cont-footer>span>i')
    }

    getEditStoryFromMenu(){
        return cy.get('#Edit-Story-0')
    }
}

export default createStory;