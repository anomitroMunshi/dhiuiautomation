/// <reference types="cypress" />

class LoginPage{

    getEmail(){
       return cy.get('#signin-email')
    }

    getPassword(){
      return  cy.get('#signin-password')
    }

    getSigninButton(){
       return cy.get('button[type="submit"]')
    }

    getnotification(){
        return cy.get('.dhi-toast > a')
    }

}
export default LoginPage;