class Rule{

createRuleBtn(){
    return cy.get('button[class="custom-btn btn neo-btn btn-primary"]')
}

getRuleNameInputBox(){
    return cy.get('#rule-name')
}

getStoryAds(){
    return cy.get('img')
}

get1stStoryAdinStoryAdSpace(){
    return cy.get('.story-ad-space>div').eq(1)
}

getSelecStoryAdDoneBtn(){
    return cy.get('#save-ads')
}

saveRule(){
    return cy.get('#save-rule')
}

backtoTagPage(){
    return cy.get('#back-to-tag-page')
}

}

export default Rule;