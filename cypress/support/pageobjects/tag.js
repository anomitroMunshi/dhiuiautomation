class tagPage{

    getCreatetagBtn(){
        return cy.get('#create-tag')
    }

    getTagNameinputBox(){
        return cy.get('#tag-name')
    }

    getCampaignNameLabel(){
        return  cy.get('#add-campaign')
    }

    getCampaignNameinputbox(){
        return cy.get('input[class="form-control dark-input url-drop"]')
    }

    adsdLandingPageURL(){
        return cy.get('#add-landingPageUrl')
    }
    getLandingPageInputBox(){
        return cy.get('#add-url')
    }

    getAddUrlBtn(){
        return cy.get('#add-url-btn')
    }
    getsavetag(){
        return cy.get('#save-tag')
    }


    getTagList(){
        return cy.get('div[class="campaign-list-container col-sm-12 col-lg-12 mg-t-10 pl-0 pr-0"]')
    }

    getEditTagOption(){
        return cy.get('.options>#Edit-Tag')
    }

    searchTag(){
        return cy.get('#search-tag')
    }

    getfirstTagName(){
        return cy.get('div[class="campaign-card-header--title"]')
    }

}

export default tagPage;