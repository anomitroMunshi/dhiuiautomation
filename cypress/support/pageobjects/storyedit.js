class storyedit{

    getAddFirstframeBtn(){
        return cy.get('#first-frame')
    }

    getStoryTitle(){
        return cy.get('#story-edit-name')
    }

    getStoryEditCloseBTN(){
        return cy.get('div[class="page-header"] span[class="close"]')
    }


    getDesignframeBTN(){
        return cy.get('#design-0')
    }

    getAddFrameLeftBtn(){
        return cy.get('#add-frame-lt-0')
    }

    //sidebar for uploading frames

    getUploadTAB(){
        return cy.get('#frames-tab__BV_tab_controls_>.nav-item:nth-child(4)')
    }

    getUploadFrameBtn(){
        return cy.get('#__BVID__46 > [style=""] > .uploads > .drop-zone > .upload-file--container > .example-drag > .upload > .uploads-border > :nth-child(1) > .text-center > #insta-frame-upload')
    }

    getUploadListBox(){
        return cy.get('div[id="__BVID__46"] div div[class="drop-zone overflow-y-scroll"]')
    }

    getUploadFrameBtn(){
        return cy.get('#__BVID__46 > [style=""] > .uploads > .drop-zone > .upload-file--container > .example-drag > .upload > .example-btn > .mb-3 > :nth-child(1)')
    }

    getUploadedFrameList(){
        return cy.get('#upload-frame-list').eq(0)
    }


    getAddframeBtn(){
        return cy.get('button[class="btn back-btn btn neo-btn add-staged-frames btn-light"]')
    }

    getClosebutton(){
        return cy.get('div[class="add-frame-sidebar"] span[class="close"]')
    }

    getStoryCreationDone(){
        return cy.get('#story-creation-done')
    }


    //


}
export default storyedit;