/// <reference types="cypress-xpath" />

class dashboard{
    getTitle(){
        return cy.get('.title')
    }

    getLogo(){
        return cy.get('.dhi-logo')
    }

    getBrandsPage(){
        return cy.get('#Brands')
    }

    getHomePage(){
       return cy.get('#Home')
    }

    getStoriespage(){
        return cy.get('#Stories')
    }

    getStoriesAdPage(){
        return cy.get('span[id="Story Ads."]')
    }

    getTagPage(){
        return cy.get('#Tags')
    }

    getInsightsPage(){
        return cy.get('#Insights')
    }


    getBrandAvatar(){
        return cy.get('.brand-avatar')
    }

    getuserinfo(){
        return cy.get('.user-info')
    }

    getBrandsPageAlt(){
        return cy.get(':nth-child(2) > :nth-child(1) > .item-name > .item-icon')
    }

    getStoryAdsPageAlt(){
        return cy.get(':nth-child(4) > :nth-child(1) > .item-name > .item-icon')
    }

    getTagPageAlt(){
        return cy.get(':nth-child(5) > :nth-child(1) > .item-name > .item-icon')
    }

    

}

export default dashboard;