class createAccount{

    getpageTitle(){
        return cy.get('div[class="page--title"]')
    }

    getcreateAccountbtn(){
        return cy.get('#create-user-btn')
    }

    getAccountNamebox(){
        return cy.get('#acc-name')
    }

    getAccountAdminName(){
        return cy.get('#admin-name')
    }

    getAccountAdminEmail(){
        return cy.get('#admin-email')
    }

    getTimezoneBox(){
        return cy.get('#select2-select-timezone-container')
    }

    getTimezoneInputbox(){
        return cy.get('.select2-search__field')
    }

    selectTimezoneForAccountCreation(){
        return cy.get('#select-timezone')
    }

    getcreateAccountUserBtn(){
        return cy.get('#create-acc-user')
    }

    getcancelAccCreation(){
        return cy.get('#cancel-acc')
    }

    getFirstAccountName(){
        return cy.get('.account-list>.list-item:nth-child(1)>.list-header>p')
    }

    getFirstbrand(){
        return cy.get('.account-list>.list-item:nth-child(1)>#brand-0>div>p')
    }

    getSuccessNotification(){
        return cy.get('.dhi-toast > .title')
    }


    //Brands

    getBrandSettingButton(){
        return cy.get('#sub-menu-0')
    }

    getEditBrandbutton(){
        return cy.get('#edit-br-0')
    }

    uploadfiles(){
        return cy.get('#upload-files')
    }

    deleteLogos(){
        return cy.get('span[class="close-btn btn-danger"]')
    }

    getLandingPageURLBox(){
        return cy.get('#landingPageUrl')
    }

    saveLandingPageURL(){
        return cy.get('#addLandingUrl')
    }

    saveEditBrandDetails(){
        return cy.get('#save-brand')
    }

    closeEditBrandbtn(){
        return cy.get('#sub-menu-0>svg')
    }

    countLogos(){
        return cy.get('.account-list>.list-item:nth-child(1)>#brand-0>div>.logo')
    }



    //Account settings

    getaccountsetiings(){
        return cy.get('#menu-0 > svg')
    }

    getOptionsinAccSettings(){
        return cy.get('div[class="option"]')
    }

    getAddAccUserBtn(){
        return cy.get('div[class="option"]>#user-0')
    }

    getAddUserBtn(){
        return cy.contains('Add User')
    }

    getObsusername(){
        return cy.get('#brand-username')
    }

    getObsemail(){
        return cy.get('#brand-email')
    }

    selectuserRole(){
        return cy.get('#select2-brand-userrole-container')
    }

    getUserRoleInputBox(){
        return cy.get('.select2-search__field')
    }

    getAccUserRegisterBtn(){
        return cy.get('#brand-user-register')
    }



}

export default createAccount;