import 'cypress-file-upload';


// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
 /// <reference types="cypress" />
 import storyedit from '../support/pageobjects/storyedit.js'
 import storyAds from '../support/pageobjects/storyAds.js'

 const storycreate=new storyedit();
Cypress.Commands.add('uplaodFrames',function(){
    
        storycreate.getUploadListBox().scrollIntoView()
        
       
        storycreate.getUploadListBox().attachFile('instaframes/Frame'+getRandomNum()+'.png', { subjectType: 'drag-n-drop' })
       .attachFile('instaframes/Frame'+getRandomNum()+'.png', { subjectType: 'drag-n-drop' })
       .attachFile('instaframes/Frame'+getRandomNum()+'.png', { subjectType: 'drag-n-drop' })
       
       
       storycreate.getUploadFrameBtn().scrollIntoView().should('be.visible').then(()=>{
        storycreate.getUploadFrameBtn().click()
       })
       cy.wait(2000)
       storycreate.getClosebutton().click({force:true})
   
}) 

function getRandomNum(min, max){
   var max=10
   var min=1
    var ran= Math.floor(Math.random() * (max - min) + min);
    
    console.log("hey**************************see meee"+ran)
    return ran;
}


Cypress.Commands.add('selectStoryAd',function(StorySize,no_of_storyADs){
    const storyAd=new storyAds();
    
    if(StorySize==='300x600'){
        storyAd.get300x600Btn().click()
    }
    else{
        storyAd.get300x250Btn().click()
    }
    
    storyAd.getStoryAdThemesList().children().each(($e,i,$list)=>{
        cy.log("value of i="+i)
        if(i<no_of_storyADs){
            $e.click()
        }
    })
})





//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
