/// <reference types="cypress" />
/// <reference types="cypress-xpath" />

import "cypress-localstorage-commands"
import Login from '../support/pageobjects/Login.js'
import Dashboard from '../support/pageobjects/Dashboard.js'
import createAccount from '../support/pageobjects/createAccount.js'
import createStory from '../support/pageobjects/createStory.js'
import storyedit from '../support/pageobjects/storyedit.js'
import storyAds from '../support/pageobjects/storyAds.js'
import tagPage from '../support/pageobjects/tag.js'
import Rule from '../support/pageobjects/rule.js'


let user;


describe('Regression Test',function(){

    before(function(){
        cy.visit(Cypress.env('url'))
        cy.clearLocalStorageSnapshot();
        cy.task("freshUser").then((object) => {
            user = object;
        });
       
          
    })


    beforeEach(function(){
        cy.restoreLocalStorage();
        cy.fixture('LoginDetails').then(function(data){
            this.data=data
        })
        
    })

    afterEach(() => {
        cy.saveLocalStorage();
      });
    
     


    

    it('Login into the application with wrong username/Password',function() {
        const login=new Login();
        cy.log("Logging with wrong username & password")
        login.getEmail().type(this.data.WrongUserName)
        login.getPassword().type(this.data.Wrongpass)
        login.getSigninButton().click()
        login.getnotification().should('contain', 'Failed!')
        cy.wait(1000)

    })

    it('Test to Login into the application ',function() {
        const login=new Login();
        const dash=new Dashboard();
        
       cy.log("Logging with correct username & password")
        login.getEmail().clear()
        login.getPassword().clear()
        login.getEmail().type(this.data.CorrectUsername)
        login.getPassword().type(this.data.CorrectPassword)
        login.getSigninButton().click()

        cy.log("Dashboard test")
        dash.getTitle().should('be.visible')
        dash.getLogo().should('be.visible')
        cy.getLocalStorage("default_auth_token").should('exist')
        //cy.genRandomNum()

        
    })



    it.skip('Test to check all basic components are present in dashboard and traverse to account creation page ',function(){

        const dash=new Dashboard();
        dash.getTitle().should('be.visible')
        dash.getLogo().should('be.visible')
        dash.getBrandAvatar().should('be.visible')
        dash.getHomePage().should('be.visible')
        dash.getBrandsPage().should('be.visible')
        dash.getStoriespage().should('be.visible')
        dash.getStoriesAdPage().should('be.visible')
        dash.getTagPage().should('be.visible')
        dash.getInsightsPage().should('be.visible')
        //dash.getBrandsPage().click()
        cy.getLocalStorage("default_auth_token").should('exist')
    })

    it('Create new Account',function(){
        const account=new createAccount();
        const dash =new Dashboard()

        dash.getBrandsPageAlt().click()
        account.getpageTitle().should('have.text','Brands')
      
        cy.log("Account creation!")
        var Accountname=user.accountName
        var Accountusername=user.username
        var useremail=user.email
        cy.log('Creating accountname with '+Accountname)
        cy.log('Creating username with '+Accountusername)
        cy.log('Creating userEmail with '+useremail)

        for(var i=0;i<2;i++){
            account.getcreateAccountbtn().click()
            if(i==0){
                cy.log("Chcking Cancel button is working or not")
                account.getcancelAccCreation().click()
                cy.wait(1000)
            }
            else{
                account.getAccountNamebox().click()
                account.getAccountNamebox().type(Accountname)
        
                account.getAccountAdminName().click({force: true})
                account.getAccountAdminName().type(Accountusername)
        
                account.getAccountAdminEmail().click({force: true})
                account.getAccountAdminEmail().type(useremail)
        
                account.getTimezoneBox().click()
                account.getTimezoneInputbox().type('Kolkata').then(function(){
                    account.getTimezoneInputbox().type('{downarrow}{enter}')
                })

                cy.wait(1000)
                account.getcreateAccountUserBtn().click()
            }
        }
        
        cy.wait(5000)
        dash.getBrandsPageAlt().click()
        
        cy.log("Check for 1st account")
        
        account.getFirstAccountName().should(($div) => {
       
            expect($div.get(0).innerText).to.include(Accountname.toUpperCase())
          })
        
          cy.log("Check for 1st Brand in 1st account")

          account.getFirstbrand().should('contain',Accountname)
          /* account.getFirstbrand().should(($div) => {

            expect($div.get(0).innerText).to.include(Accountname)
          }) */

        cy.getLocalStorage("default_auth_token").should('exist')
})

    it('Edit Brand',function(){
        const account=new createAccount();
        const dash =new Dashboard()

        dash.getBrandsPageAlt().click()

        account.getBrandSettingButton().click({force: true}).then(()=>{
        account.getEditBrandbutton().click()
     })
    
    cy.wait(2000)
    
    account.uploadfiles().attachFile('Logo2.png').attachFile('Logo1.png').attachFile('Logo3.png').attachFile('Logo4.png').attachFile('Logo5.png')
    cy.wait(1000)
   
    var landingUrl= user.landingpageurl
    cy.log("Landing Page URL  : "+landingUrl)
    account.getLandingPageURLBox().type(landingUrl)
    account.saveLandingPageURL().click()
    account.saveEditBrandDetails().click()
    cy.wait(3000)
    dash.getBrandsPageAlt().click()
    cy.wait(5000)
    cy.reload()
    dash.getBrandsPageAlt().click()
    account.countLogos().find('img').should('have.length',5)
   //Logo Deletion
   /* account.deleteLogos().each(($e,i,$list)=>{
    if(i==0){
        account.deleteLogos().eq(i).click()
    }
}) */
    cy.getLocalStorage("default_auth_token").should('exist')
    
})

    it('Adding user to the 1st Account',function(){
    const account=new createAccount();
    const dash =new Dashboard()

    dash.getBrandsPageAlt().click()

    var observername=user.username_obs
    cy.log(observername)
    var observeremail=user.email_obs
    cy.log(observeremail)

    account.getaccountsetiings().click()
    account.getOptionsinAccSettings().find('button').should('have.length',3)
    account.getAddAccUserBtn().click()
    account.getAddUserBtn().click()

    account.getObsusername().click()
    account.getObsusername().type(observername)

    account.getObsemail().click()
    account.getObsemail().type(observeremail)

    account.selectuserRole().click({force:true})

    account.getUserRoleInputBox().type('Account Observer').then(()=>{
        account.getUserRoleInputBox().type('{downarrow}{enter}')
    })
    cy.wait(2000)
    account.getAccUserRegisterBtn().click()

    })

    it('Create Story',function(){
        const dash =new Dashboard()
        const account=new createAccount();
        const story=new createStory();
        const storycreate=new storyedit();


        dash.getBrandsPageAlt().click()
        account.getFirstbrand().click()
        cy.wait(2000)
        story.getStoryPageTitle().should('contain','Stories')

        story.getCreateStoryBtn().click()

        var storyname=user.StoryName
        var localstoryname=user.StoryLocalname

        story.getStoryDisplayNamebox().type(storyname,{force:true})
        story.getLocalName().type(localstoryname)
        story.getCreatebtn().click()
        cy.wait(1000)
        storycreate.getAddFirstframeBtn().should('be.visible').and('be.enabled').then(()=>{
            storycreate.getStoryEditCloseBTN().click()
        })




    })

    it('Create/Edit Story',function(){
        const account=new createAccount();
        const story=new createStory();
        const storycreate=new storyedit();
        const dash=new Dashboard();

        var storyname=user.StoryName
        var localstoryname=user.StoryLocalname


        dash.getBrandsPageAlt().click()
        account.getFirstbrand().click()
        cy.wait(2000)
        story.getStoryPageTitle().should('contain','Stories')
        story.getStoryList().children().then(($e)=>{
        const len=$e.length
         cy.log("No.of Stories--> "+len)
             if(parseInt(len)>0)
             {
                story.getFirstStorynameFromList().then(($name)=>{
                const firststoryname=$name.text()
                cy.log("first story name->"+firststoryname)
                
                }).then(()=>{

                story.getFirstStoryMenu().click()
                story.getEditStoryFromMenu().click()
                })  
            }
            else
            {
             cy.log("yo")
             story.getCreateStoryBtn().click()
             story.getStoryDisplayNamebox().type(storyname,{force:true})
             story.getLocalName().type(localstoryname)
             story.getCreatebtn().click()
             cy.wait(1000)
             }   
        storycreate.getAddFirstframeBtn().should('be.visible').and('be.enabled')
        storycreate.getStoryTitle().should('be.visible')
        storycreate.getAddFirstframeBtn().click()
        storycreate.getUploadTAB().click()
        storycreate.getUploadedFrameList().then(function($f){
            if(!Cypress.dom.isVisible($f)){
                //cy.log("Notpresent")
                cy.uplaodFrames()

            }
            /* else{

                storycreate.getUploadedFrameList().find('div').then(function(e){
                    var len=e.length
                    cy.log("no.of existong frames--->"+ len)
                        cy.log("Frames present, proceeding to story creation directly")
                        storycreate.getClosebutton().click({force:true})
                })
                
             } */
        })
       
       })
    })

    it('Add Frames to the story',function(){
        const storycreate=new storyedit();

        cy.wait(2000)

        storycreate.getAddFirstframeBtn().click()
        storycreate.getUploadTAB().click()
        storycreate.getUploadedFrameList().find('div').each(($e,i,$list)=>{
                if(i<3){
                    $e.click()
                }
        })
        storycreate.getAddframeBtn().click()
        cy.wait(3000)
        storycreate.getStoryCreationDone().click()



       /*  storycreate.getDesignframeBTN().then(function($el){
            if(Cypress.dom.isVisible($el)){
                cy.log("i'm there!!!!")
                storycreate.getAddFrameLeftBtn().click()
                storycreate.getUploadTAB().click()
                storycreate.getUploadedFrameList().find('div').each(($e,i,$list)=>{
                        if(i<2){
                            $e.click()
                        }
                })
                storycreate.getAddframeBtn().click()
                cy.wait(3000)
                storycreate.getStoryCreationDone().click()

            }
            else{
                storycreate.getAddFirstframeBtn().click()
                storycreate.getUploadTAB().click()
                storycreate.getUploadedFrameList().find('div').each(($e,i,$list)=>{
                        if(i<3){
                            $e.click()
                        }
                })
                storycreate.getAddframeBtn().click()
                cy.wait(3000)
                storycreate.getStoryCreationDone().click()

            }
        }) */
    })

    it('StoryAd creation',function(){

        const dash=new Dashboard();
        const storyAd=new storyAds();

        dash.getStoryAdsPageAlt().click()
        storyAd.getPageTitle().should('contain','Story Ads')
        storyAd.getCreateStoryAdsBtn().click()
        cy.reload()
        storyAd.getFirstStory().click()
        storyAd.getSelectedicon().should('be.visible')
        storyAd.getApplyBtnWhileCreatingStory().click()

        storyAd.getFrameSelectionList().find('div').then(function($e){
            const no_of_frames=$e.length
            cy.log("No.of Frames found = "+no_of_frames)
            if(no_of_frames>0){
                storyAd.getFrameSelectionList().find('div').each(($e,i,$list)=>{
                    $e.click()
                })
            }
        })
        storyAd.getStoryAdFrameApply().click()

        //edit storyAd contents

        storyAd.selectThirdLogo().click()
        storyAd.getinputBoxForStoryAd().type("This is a Test")
        storyAd.getAddTextlabel().click()
        storyAd.getSubtitileinputBoxforStoryAd().type("This is a subtitle test")
        storyAd.getFontContainer().click()
        storyAd.getFontInputBox().type('Jaldi').then(function(){
            storyAd.getFontInputBox().type('{downarrow}{enter}')
        })

        storyAd.getStoryAdThemesList().children().then(function($e){
            cy.log("no.of themes---> "+$e.length)
        })

        //Selecting 2 300x600 storyADs
        cy.selectStoryAd("300x600",2)

        //Selecting 2 300x250 storyADs
        cy.selectStoryAd("300x250",2)

        storyAd.getEditStoryAdNamelabelBtn().eq(1).click()
        storyAd.getInputboxStoryAdName().clear()
        storyAd.getInputboxStoryAdName().type("Test Story ads by cypress")
        storyAd.saveStoryAdname().click({force: true})
        storyAd.saveAds().click()
    
    })

    it('Create Tags',function(){
        const dash=new Dashboard();
        const tag=new tagPage();

        dash.getTagPageAlt().click()
        tag.getCreatetagBtn().click()
        tag.getTagNameinputBox().type(user.TagName)
        tag.getCampaignNameLabel().click()
        tag.getCampaignNameinputbox().type(user.CampaignName)

        tag.adsdLandingPageURL().click()
        tag.getLandingPageInputBox().type(user.landingpageurl)
        tag.getAddUrlBtn().click()
        tag.getsavetag().click()

    })

    it('Create Rule',function(){
        const rule=new Rule();

        rule.createRuleBtn().click()
        rule.getRuleNameInputBox().type(user.RuleName)
        rule.getStoryAds().click()

        cy.wait(3000)

        rule.get1stStoryAdinStoryAdSpace().trigger('mouseover').then(()=>{
           
            cy.get('#add-ads').invoke('show').click({force: true})
        })

        rule.getSelecStoryAdDoneBtn().click()

        cy.wait(1000)

        rule.saveRule().click()
        cy.wait(1000)
        rule.backtoTagPage().click()

       
    })

    it('Edit tag',function(){
        const dash=new Dashboard();
        const tag=new tagPage();

        dash.getTagPageAlt().click()
        tag.getTagList().children().then(()=>{
            tag.getTagList().children().eq(0).find('#tag-menu').click()
        })
        tag.getEditTagOption().eq(0).click()
        cy.wait(2000)
        var newTagName=user.TagName
        cy.log("NEw tag name="+newTagName)
        tag.getTagNameinputBox().clear()
        tag.getTagNameinputBox().type(newTagName)
        tag.getsavetag().click()
        cy.wait(2000)
        tag.searchTag().type(newTagName)
        tag.getfirstTagName().should('contain',newTagName)

    })



   
      

         
    
   



})
